$(function(){
	//separator
	var autonumOptions = {wEmpty: 'zero', aSep: '.', aDec: ',', mDec: '0', lZero: 'deny'};
	$("[name^='total']").autoNumeric(autonumOptions);
	$("[name^='total_']").change( function() {calculateTotalPrice();});
	$("table td.num_separator").autoNumeric(autonumOptions);

	$("[name^='total']").addClass('span2 text-right').attr('autocomplete','off');
    
});

function setActiveMenu(menuitem) {
    $('li[class="active"]').removeClass("active");
	$("#" + menuitem).addClass('active');
};


function init () {
    var canvas = document.getElementById("easel");
    var render = canvas.getContext("2d");

	graphics = new createjs.Graphics();
	SIZE = 100; 
	centerX = canvas.width/2;
	centerY = canvas.height/2;

	var shape = new createjs.Shape();
	graphic.beginFill();
	graphic.drawRect();

 	// var stage = 
	// stage.addChild(shape);
	// stage.update();
}

function calculateTotalPrice () {
	var totalPrice = 0;

	$("[name^='total_']").each(function(index, object) {
		console.log($(this).autoNumeric('get'));
    	totalPrice = parseInt($(this).autoNumeric('get')) + parseInt(totalPrice);
	});

	console.log("totalPrice: " + parseInt(totalPrice));
	$("#total").autoNumeric('set', totalPrice); 
}