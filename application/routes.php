<?php

// user Resource
Route::get('users', array('as' => 'users', 'uses' => 'users@index'));
Route::get('users/(:any)', array('as' => 'user', 'uses' => 'users@show'));
Route::get('users/new', array('as' => 'new_user', 'uses' => 'users@new'));
Route::get('users/(:any)/edit', array('as' => 'edit_user', 'uses' => 'users@edit'));
Route::post('users', 'users@create');
Route::put('users/(:any)', 'users@update');
Route::delete('users/(:any)', 'users@destroy');
// custom add
Route::get('search_users', 'users@index');
Route::post('search_users', 'users@index');

// customer Resource
Route::get('customers', array('as' => 'customers', 'uses' => 'customers@index'));
Route::get('customers/(:any)', array('as' => 'customer', 'uses' => 'customers@show'));
Route::get('customers/new', array('as' => 'new_customer', 'uses' => 'customers@new'));
Route::get('customers/(:any)/edit', array('as' => 'edit_customer', 'uses' => 'customers@edit'));
Route::post('customers', 'customers@create');
Route::put('customers/(:any)', 'customers@update');
Route::delete('customers/(:any)', 'customers@destroy');
// custom add
Route::get('search_customers', 'customers@index');
Route::post('search_customers', 'customers@index');

// salesorder Resource
Route::get('salesorders', array('as' => 'salesorders', 'uses' => 'salesorders@index'));
Route::get('salesorders/(:any)', array('as' => 'salesorder', 'uses' => 'salesorders@show'));
Route::get('salesorders/new', array('as' => 'new_salesorder', 'uses' => 'salesorders@new'));
Route::get('salesorders/(:any)/edit', array('as' => 'edit_salesorder', 'uses' => 'salesorders@edit'));
Route::get('salesorders/search', array('as' => 'search_salesorders', 'uses' => 'salesorders@search'));
Route::post('salesorders', 'salesorders@create');
Route::put('salesorders/(:any)', 'salesorders@update');
Route::delete('salesorders/(:any)', 'salesorders@destroy');
// custom add
Route::get('search_salesorders', 'salesorders@index');
Route::post('search_salesorders', 'salesorders@index');

Route::get('/', function()
{
	// $user = User::find(1);
	// $user->password = 'password';
	// $user->save();
	// return 'hash pswd:' . $user->password;
	if (Auth::guest()) return Redirect::to('login');
	else return View::make('home.index');
});

Route::get('login', function () {
	return View::make('home.login');
});

Route::post('login', function () {
	$credentials = array(
		'username' => Input::get('username'),
		'password' => Input::get('password'),
		'remember' => true,
	);

	if(Auth::attempt($credentials)) {
		if (Auth::check()) return View::make('home.index');
	}

	return View::make('home.login')->with('error' , ' Username / Password salah');

});

Route::get('logout',function(){
	Auth::logout();
	//redirect to login
	return View::make('home.login');
});

// Route::post('/', function () {
// 	return View::make('home.index');
// });

Route::get('dump1234', function()
{
	$user = User::find(1);
	$user->password = 'password';
	$user->save();
	// return 'hash pswd:' . $user->password;
});

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function($exception)
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	Log::write('info', 'Auth::check' . Auth::check());
	if (Auth::guest()) return Redirect::to('login');
});