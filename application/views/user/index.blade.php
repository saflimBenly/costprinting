@layout('master')

@section('container')

	@if(Session::has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{ Session::get('success')}}
		</div>
	@endif

	@if(isset($success))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{$success}}
		</div>
	@endif

  	{{ Form::open('search_users', 'POST', array('id' => 'searchForm', 'class' => 'form-search pull-right'))}}
		<div class="input-append">
	  		<input type="text" name="searchText" id="searchText" class="input-medium search-query span3" , placeholder="Cari berdasarkan: Id/Username">
			<button type="submit" class="btn">Search</button>
		</div>
	{{ Form::close() }}

	<table class="table table-striped table-hover" id="user_list">
		<thead>
		    <tr>
		      <th width="15%" class="num_title">User Id</th>
		      <th>User Name</th>
		      <th>Bio</th>
		      <th>Menu Akses</th>
		    </tr>
	    </thead>
	    <tbody>
	  	@foreach ($users->results as $user)
			<tr>
				<td class="num_id">
					{{$user->id}}
				</td>
				<td>{{$user->name}}</td>
				<td>{{$user->bio}}</td>
				<td>This is module access</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	{{$users->links()}}
	{{HTML::link_to_route('new_user', 'New user',null, array('class'=>'btn btn-primary'))}}
	<br>
	<br>

@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_user");
		var location =  "{{URL::to_route('user')}}";

	    $("table#user_list td").not(":first").click(function(){
	        document.location = location + "/" + 
	         $(this).closest("tr").find(".num_id").text();
	    });	

    });

</script>
@endsection

