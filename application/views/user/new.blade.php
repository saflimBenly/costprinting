@layout('master')
@section('container')
		{{Form::open('/users', 'POST', array('class' => 'form-horizontal', 'id' => 'userForm'))}}

		<div class='control-group'> 
			<label class='text'>
				<span class='ilabel'>User Id</span>
				{{Form::text('id',$lastId, array('class'=> 'span1', 'id' =>'id', 'disabled' => 'disabled'))}}	
			</label>
			<label class='text'>
				<span class='ilabel'>Nama User</span>
				{{Form::text('name','', array('class'=> 'span4', 'id' => 'name'))}}	
				<span class="alert-error"></span>
			</label>
			<label class='text'>
				<span class='ilabel'>Jenis Kelamin</span>
				&nbsp;<label class='radio inline'>
				{{Form::radio('gender', 'P', 'true', array('id' => 'pria'))}} Pria
				</label>
				<label class='radio inline'>
				{{Form::radio('gender', 'W', '', array('id' => 'wanita'))}} Wanita
				</label>
			</label>
			<label class='text'>
				<span class='ilabel'>Password</span>
				{{Form::password('password', array('class'=> 'span4', 'id' => 'password'))}}
				<span class="alert-error"></span>
			</label>

			<label class='text'>
				<span class='ilabel'>Confirm Password</span>
				{{Form::password('confirmpassword', array('class'=> 'span4', 'id' => 'confirmpassword'))}}
				<span class="alert-error"></span>
			</label>

			<label class='text'>
				<span class='ilabel'>Email</span>
				{{Form::text('email','', array('class'=> 'span4', 'id' => 'email'))}}	
				<span class="alert-error"></span>
			</label>

			<label class='text'>
				<span class='ilabel'>Jabatan</span>
				{{Form::text('position','', array('class' => 'span4', 'id' => 'position'))}}	
			</label>
			<label class='text'>
				<span class='ilabel'>Keterangan</span>
				{{Form::textarea('bio','', array('class' => 'span4', 'id' => 'bio', 'row' => '4'))}}	
			</label>
			<br>
			<label class='text'>
				<span class='ilabel'>&nbsp;</span>
  				{{HTML::link_to_route('users', 'Back', '', array('class' => 'btn'))}}
  				{{HTML::link('#', 'Reset', array('id' => 'resetButton', 'class' => 'btn btn-warning'))}}
  				&nbsp;
  				{{Form::submit('Save', array('id' => 'saveButton', 'class' => 'btn btn-primary'))}}
			</label>
		</div>

	{{Form::close()}}
	
@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_user");

		$('#userForm').validate({
	        rules: {
	            name: {
	                minlength: 3,
	                required: true
	            },
	            email: {
	                required: true,
	                email: true
	            },
	            password: {
	                required: true, 
	                minlength: 5
	            },
	            confirmpassword: {
	                required: true,
	                minlength: 5, 
	                equalTo: '#password'
	            }
	        },
	        messages: {
			name: {
				required: "Silakan masukkan Nama User",
				minlength: "Nama User minimal 3 karakter"
			},
			password: {
				required: "Silakan masukkan password",
				minlength: "Password minimal 5 karakter"
			},
			confirmpassword: {
				required: "Silakan masukkan password confirm",
				minlength: "Password minimal 5 karakter",
				equalTo: "Password dan Confirm Password harus sama"
			},
			email: "Silakan masukkan alamat email dengan format yang benar"
		},
		errorPlacement: 
			function(error, element) {
				errorText = error.text();
				element.siblings('.alert-error').text(errorText).css({padding:"5px"}); 
		},
		errorElement:'span',
		highlight: 
			function(element, errorClass) {
       			$(element).focus();
  		},
		unhighlight: function (element, errorClass) {
	        $(element).siblings('.alert-error').text('').css({padding:"0px"}); 
	    },
	    onfocusout: false,
		submitHandler: function (form) {
        	$('#id').removeAttr('disabled');
        	$('form#userForm').submit();
        }
    });

		$('#resetButton').click(function () {
			$("form#userForm :input:not(:checkbox):not(:button):not([type=hidden]):not([name=id])").val('');
		});
	});
</script>
@endsection