@layout('master')

@section('container')

	@if(Session::has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{ Session::get('success') }}
		</div>
	@endif


	@if(isset($parent))
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong>
			<ul>
				{{$parent}}
			</ul>
		</div>
	@endif

<h4>user Detail</h4>
	<div class="row">
		<div class="row">
	    	<span class="span2 ilabel"><strong>User Id</strong></span>
	    	<span class="span2">{{$user->id}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Username</strong></span>
	    	<span class="span2">{{$user->name}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Bio</strong></span>
	    	<span class="span2">{{$user->bio}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Akses Menu</strong></span>
	    	<span class="span2">Dipisahkan menurut hak akses</span>
		</div>
	</div>
  	{{ Form::open('users/'.$user->id, 'DELETE', array('id' => 'userForm'))}}
  		{{HTML::link_to_route('users', 'Back', '', array('class' => 'btn'))}}
		{{HTML::link_to_route('edit_user', 'Edit', array($user->id), array('id' => 'editButton', 'class' => 'btn btn-primary'))}}
	{{ Form::close() }}

@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_user");

    });
</script>
@endsection

