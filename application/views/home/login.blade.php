    <title>Cost Printing Calculation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    {{HTML::script('js/jquery.js')}}
    {{Asset::container('bootstrapper')->styles()}}
    {{Asset::container('bootstrapper')->scripts()}}
    <!-- Le styles -->
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
        background: #F5F6F6;
      }
    </style>
    {{HTML::style('css/style.css');}}

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
</head>
    <!-- Fav and touch icons -->
  <body data-feedly-processed="yes">
    <div id="wrap">

      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#">PRINT COST CALCULATION</a>
            <div class="nav-collapse collapse">
            </div><!--/.nav-collapse -->
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          {{Form::open('/login', 'POST', array('class' => 'form-horizontal', 'id' => 'loginForm'))}}
            <div class="control-group">
              <h4>Please Login</h4>
              @if(isset($error))
                <div class="alert alert-error">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Error!</strong>{{$error}}
                </div>
              @endif              
              <label class='text'>
                <span class='ilabel'>Username</span>
                {{Form::text('username',Input::old('username'), array('class'=> 'span2', 'id' => 'username', 'placeholder' => 'Username'))}} 
              </label>
              <label class='text'>
                <span class='ilabel'>Password</span>
                {{Form::password('password', array('class' => 'span2', 'id' => 'password', 'placeholder' => 'Password'))}}
              </label>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Sign in</button>
              </div>
            </div>
            <div class="container">
              <p class="muted credit text-center">Courtesy of evLogia Solution @ 2013</p>
            </div>
          </div>
         {{Form::close()}}
        </div>
      <div id="push"></div>
  </div>

</body>
</html>