@layout('master')

@section('container')

	@if(Session::has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{ Session::get('success') }}
		</div>
	@endif

  	{{ Form::open('search_salesorders', 'POST', array('id' => 'searchForm', 'class' => 'form-search pull-right'))}}
		<div class="input-append">
	  		<input type="text" name="searchText" id="searchText" class="input-medium search-query span3" , placeholder="Cari berdasarkan: Id/Cust/Gambar">
			<button type="submit" class="btn">Search</button>
		</div>
	{{ Form::close() }}

	<table class="table table-striped table-hover" id="salesorder_list">
		<thead>
		    <tr>
		      <th width="15%" class="num_title">No.Sales Order</th>
		      <th>Cust</th>
		      <th>Nama Cetakan</th>
		      <th width="20%" class="num_title">Qty</th>
		      <th width="20%" class="num_title">Total Biaya</th>
		    </tr>
	    </thead>
	    <tbody>
	  	@foreach ($salesorders->results as $salesorder)
			<tr>
				<td class="num_id">
					{{$salesorder->salesorders_id}}
				</td>
				<td>{{$salesorder->custname}}</td>
				<td>{{$salesorder->printname}}</td>
				<td class="num_separator">{{$salesorder->qty}}</td>
				<td class="num_separator">{{$salesorder->total}}</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	{{$salesorders->links()}}
	{{HTML::link_to_route('new_salesorder', 'New Sales Order',null, array('class'=>'btn btn-primary'))}}
	<br>
	<br>

@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_salesorder");
		var location =  "{{URL::to_route('salesorder')}}";

	    $("table#salesorder_list td").not(":first").click(function(){
	        document.location = location + "/" + 
	         $(this).closest("tr").find(".num_id").text();
	    });	

    });

</script>
@endsection

