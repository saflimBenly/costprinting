@layout('master')

@section('container')
	
	@if(Session::has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{ Session::get('success') }}
		</div>
	@endif

	@if($errors->has())
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong>
			<ul>
				{{$errors->first('id', '<li>:message</li>')}}
				{{$errors->first('custname', '<li>:message</li>')}}
			</ul>
		</div>
	@endif

	

	{{Form::open('salesorders/'.$salesorder->id, 'PUT', array('class' => 'form-horizontal', 'id' => 'salesorderForm'))}}
		<div class='control-group'> 
			<label class='text'>
				<span class='ilabel'>No. Order</span>				
				{{Form::text('id', $salesorder->id, array('class'=> 'span1', 'id' => 'id', 'disabled' => 'disabled'))}}	
			</label>
			<label class='text'>
				<span class='ilabel'>Nama Customer</span>
				{{Form::text('custname', $salesorder->customer->id .'-'. $salesorder->customer->company, array('class'=> 'span4', 'id' =>'custname', 'disabled' => 'disabled'))}}
			</label>
			<label class='text'>
				<span class='ilabel'>Nama Cetakan</span>
				{{Form::text('printname',$salesorder->printname, array('class'=> 'span4'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Gambar</span>
				{{Form::text('picturedimension',$salesorder->picturedimension, array('class'=> 'span4'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Plat</span>
				{{Form::text('plat',$salesorder->plat, array('class'=> 'span4'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Cetak</span>
				{{Form::text('qty',$salesorder->qty, array('class'=> 'span3', 'id' => 'qty'))}}
			</label>

			<label class='text'>
				<span class='ilabel'>Finishing</span>
				{{Form::select('finishing', array('blank' => '', 'laminating' => 'Laminating', 'doff' => 'Doff','glossy' => 'Glossy', 'uv' => 'UV', 'spotuv' => 'Spot UV', 'lem' => 'Lem', 'emboss' => 'Emboss', 'poly' => 'Poly'), $salesorder->finishing, array('class' => 'span2'))}}
			</label>

			<label class='text'>
				<span class='ilabel'>&nbsp;</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_finishing',$salesorder->total_finishing, array('class' => 'text-right', 'id' => 'total_finishing'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Pisau</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_pisau',$salesorder->total_pisau, array('class' => 'span2 text-right', 'id' => 'total_pisau'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Pon</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_pon',$salesorder->total_pon, array('class' => 'span2 text-right', 'id' => 'total_pon'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Potong</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_potong',$salesorder->total_potong, array('class' => 'span2 text-right', 'id' => 'total_potong'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Bor</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_bor',$salesorder->total_bor, array('class' => 'span2 text-right', 'id' => 'total_bor'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Mata Ayam</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_mataayam',$salesorder->total_mataayam, array('class' => 'span2 text-right', 'id' => 'total_mataayam'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Tali</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_tali',$salesorder->total_tali, array('class' => 'span2 text-right', 'id' => 'total_tali'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Klise</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_klise',$salesorder->total_klise, array('class' => 'span2 text-right', 'id' => 'total_klise'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			{{Form::text('other', $salesorder->other, array('class'=>"otherLabel", "placeholder"=> "Input other"))}}	

			<label class='text otherLabel'>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_other',$salesorder->total_other, array('class' => 'span2 text-right', 'id' => 'total_other'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>
			<label class='text'></label>
			<label class='text'>
				<span class='ilabel'><strong>Total Biaya</strong></span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total',$salesorder->total, array('class' => 'span2 text-right', 'id' => 'total', 'disabled' => 'disabled'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>
			<br>
			<label class='text'>
				<span class='ilabel'>&nbsp;</span>
  				{{HTML::link(Request::referrer(), 'Back', array('class' => 'btn'))}}
  				{{HTML::link('#', 'Save', array('id' => 'saveButton', 'class' => 'btn btn-primary'))}}
			</label>
		</div>

	{{Form::close()}}

	<div id="drawingpad">@include('salesorder.canvas')</div>
@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_salesorder");

		var autonumOptions = {wEmpty: 'zero', aSep: '.', aDec: ',', mDec: '0'};
		$("[name='qty']").autoNumeric(autonumOptions);
		$("[name='qty']").attr("style", "text-align:right");

		$('#saveButton').click(function(){ 
			var temp = 0;
			
			$('#id').removeAttr('disabled');
			$('#custname').removeAttr('disabled');
			$('#total').removeAttr('disabled');

			$("[name^='total']").each(function(index, object) {
				temp = parseInt($(this).autoNumeric('get'));
				$(this).autoNumeric('destroy');
				$(this).val(temp);
				console.log('Temp: ' + temp);
			});

			//cetak juga
			temp = parseInt($('#qty').autoNumeric('get'));
			$('#qty').autoNumeric('destroy');
			$('#qty').val(temp);

			$('form#salesorderForm').submit();
		});
    });
</script>
@endsection	