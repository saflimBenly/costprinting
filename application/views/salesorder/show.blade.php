@layout('master')

@section('container')

	@if(Session::has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{ Session::get('success') }}
		</div>
	@endif

<h4>Sales Order Detail</h4>
	<div class="row">
		<div class="row">
	    	<span class="span2 ilabel"><strong>No. Order</strong></span>
	    	<span class="span2">{{$salesorder->id}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Nama Customer</strong></span>
	    	<span class="span2">{{$salesorder->customer->custname}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Nama Cetakan</strong></span>
	    	<span class="span2">{{$salesorder->printname}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Dimensi Gambar</strong></span>
	    	<span class="span2">{{$salesorder->picture}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Plat</strong></span>
	    	<span class="span2">{{$salesorder->plat}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Jumlah Cetak</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->qty}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Finishing</strong></span>
	    	<span class="span1">{{$salesorder->finishing}}</span>
	    	<span class="span1" name="total_num">{{$salesorder->total_finishing}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Pisau</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total_pisau}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Pon</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total_pon}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Potong</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total_potong}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Bor</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total_bor}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Mata Ayam</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total_mataayam}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Tali</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total_tali}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Klise</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total_klise}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>{{$salesorder->other}}</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total_other}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>TOTAL</strong></span>
	    	<span class="span2" name="total_num">{{$salesorder->total}}</span>
		</div>
	</div>
  	{{ Form::open('salesorders/'.$salesorder->id, 'DELETE', array('id' => 'salesorderForm'))}}
  		{{HTML::link_to_route('salesorders', 'Back', '', array('class' => 'btn'))}}
		{{HTML::link_to_route('edit_salesorder', 'Edit', array($salesorder->id), array('id' => 'editButton', 'class' => 'btn btn-primary'))}}
		{{HTML::link('#', 'Delete', array('id' => 'deleteButton', 'class' => 'btn btn-danger'))}}
	{{ Form::close() }}

    <!--@if ( !Auth::guest() ) -->
    <!--@endif-->

	<!-- Modal -->
	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel">Delete Record</h3>
		</div>
		<div class="modal-body">
			<p>Yakin Hapus record ini? (id = {{$salesorder->id}})</p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
			<button class="btn btn-danger" id="deleteConfirm">Delete</button>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_salesorder");

		$('#deleteButton').click(function(){
			$('#myModal').modal('show');
		});

		$('#deleteConfirm').click(function(){
			$('form#salesorderForm').submit();
			$('#myModal').modal('hide');
			return false;
		});
    });
</script>
@endsection

