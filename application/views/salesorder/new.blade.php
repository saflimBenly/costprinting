@layout('master')

@section('container')
	{{Form::open('/salesorders', 'POST', array('class' => 'form-horizontal', 'id' => 'salesorderForm'))}}

		<div class='control-group'> 
			<label class='text'>
				<span class='ilabel'>No. Order</span>
				{{Form::text('id',$lastId, array('class'=> 'span1', 'id' =>'id', 'disabled' => 'disabled'))}}	
			</label>
			<label class='text'>
				<span class='ilabel'>Nama Customer</span>
				{{Typeahead::create($getCustomerForSelect, 5, array('class'=> 'typeahead span4', 'data-provide'=>'typeahead', 'id'=> 'custname', 'autocomplete' => 'off'))}}
				{{Form::hidden('custId', '', array('id' => 'custId'))}}
			</label>
			<label class='text'>
				<span class='ilabel'>Nama Cetakan</span>
				{{Form::text('printname','', array('class'=> 'span4'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Gambar</span>
				{{Form::text('picturedimension','', array('class'=> 'span4'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Plat</span>
				{{Form::text('plat','', array('class'=> 'span4'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Cetak</span>
				{{Form::text('qty','0', array('id' => 'qty'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Finishing</span>
				{{Form::select('finishing', array('blank' => '', 'laminating' => 'Laminating', 'doff' => 'Doff','glossy' => 'Glossy', 'uv' => 'UV', 'spotuv' => 'Spot UV', 'lem' => 'Lem', 'emboss' => 'Emboss', 'poly' => 'Poly'), '', array('class' => 'span2'))}}
			</label>

			<label class='text'>
				<span class='ilabel'>&nbsp;</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_finishing','0', array('class' => 'text-right', 'id' => 'total_finishing'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Pisau</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_pisau','0', array('id' => 'total_pisau'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Pon</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_pon','0', array('id' => 'total_pon'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Potong</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_potong','0', array('id' => 'total_potong'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Bor</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_bor','0', array('id' => 'total_bor'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Mata Ayam</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_mataayam','0', array('id' => 'total_mataayam'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Tali</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_tali','0', array('id' => 'total_tali'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			<label class='text'>
				<span class='ilabel'>Klise</span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_klise','0', array('id' => 'total_klise'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>

			{{Form::text('other', '', array('class'=>"otherLabel", "placeholder"=> "Input other"))}}	

			<label class='text otherLabel'>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total_other','0', array('id' => 'total_other'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>
			<label class='text'></label>
			<label class='text'>
				<span class='ilabel'><strong>Total Biaya</strong></span>
				<div class='input-prepend input-append'>
					<span class='add-on'>Rp</span>
					{{Form::text('total','0', array('id' => 'total', 'disabled' => 'disabled'))}}	
					<span class='add-on'>,00</span>
				</div>
			</label>
			<br>
			<label class='text'>
				<span class='ilabel'>&nbsp;</span>
  				{{HTML::link_to_route('salesorders', 'Back', '', array('class' => 'btn'))}}
  				{{HTML::link('#', 'Reset', array('id' => 'resetButton', 'class' => 'btn btn-warning'))}}
  				&nbsp;
  				{{HTML::link('#', 'Save', array('id' => 'saveButton', 'class' => 'btn btn-primary'))}}
			</label>
		</div>

	{{Form::close()}}
	

	<div id="drawingpad">@include('salesorder.canvas')</div>
@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_salesorder");

		var autonumOptions = {wEmpty: 'zero', aSep: '.', aDec: ',', mDec: '0', lZero: 'deny'};
		$("[name='qty']").autoNumeric(autonumOptions);
		$("[name='qty']").attr("style", "text-align:right");

        $('#custname').typeahead({
            source : {{$getCustomerForSelect}}
        });

		$('#saveButton').click(function(){
			$('#id').removeAttr('disabled');
			$('#total').removeAttr('disabled');

			if ($('#custname').val() === '') {
				alert('Silakan masukkan Nama Customer');
				return false;
			}
			var custname = $('#custname').val().split("-",1);

			$('#custId').val(custname[0]);

			var temp = 0;

			$("[name^='total']").each(function(index, object) {
				temp = parseInt($(this).autoNumeric('get'));
				$(this).autoNumeric('destroy');
				$(this).val(temp);
			});

			//cetak/qty juga
			temp = parseInt($('#qty').autoNumeric('get'));
			$('#qty').autoNumeric('destroy');
			$('#qty').val(temp);

			$('form#salesorderForm').submit();
			return false;
		});        

		$('#resetButton').click(function () {
			$("form#salesorderForm :input:not(:checkbox):not(:button):not([type=hidden]):not([name=id])").val('');
		});
	});

</script>
@endsection