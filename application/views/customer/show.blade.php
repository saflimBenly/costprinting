@layout('master')

@section('container')

	@if(Session::has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{ Session::get('success') }}
		</div>
	@endif


	@if(isset($parent))
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong>
			<ul>
				{{$parent}}
			</ul>
		</div>
	@endif

<h4>Customer Detail</h4>
	<div class="row">
		<div class="row">
	    	<span class="span2 ilabel"><strong>Customer Id</strong></span>
	    	<span class="span2">{{$customer->id}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Nama Customer</strong></span>
	    	<span class="span2">{{$customer->custname}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Nama Perusahaan</strong></span>
	    	<span class="span2">{{$customer->company}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Alamat</strong></span>
	    	<span class="span2">{{$customer->address}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Telp.</strong></span>
	    	<span class="span2">{{$customer->telp}}</span>
		</div>
		<div class="row">
	    	<span class="span2 ilabel"><strong>Email</strong></span>
	    	<span class="span2">{{$customer->email}}</span>
		</div>
	</div>
  	{{ Form::open('customers/'.$customer->id, 'DELETE', array('id' => 'customerForm'))}}
  		{{HTML::link_to_route('customers', 'Back', '', array('class' => 'btn'))}}
		{{HTML::link_to_route('edit_customer', 'Edit', array($customer->id), array('id' => 'editButton', 'class' => 'btn btn-primary'))}}
		{{HTML::link('#', 'Delete', array('id' => 'deleteButton', 'class' => 'btn btn-danger'))}}
	{{ Form::close() }}

    <!--@if ( !Auth::guest() ) -->
    <!--@endif-->

	<!-- Modal -->
	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel">Delete Record</h3>
		</div>
		<div class="modal-body">
			<p>Yakin Hapus record ini? (id = {{$customer->id}})</p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
			<button class="btn btn-danger" id="deleteConfirm">Delete</button>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_customer");

		$('#deleteButton').click(function(){
			$('#myModal').modal('show');
		});

		$('#deleteConfirm').click(function(){
			$('form#customerForm').submit();
			$('#myModal').modal('hide');
			return false;
		});
    });
</script>
@endsection

