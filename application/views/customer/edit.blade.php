@layout('master')

@section('container')
	
	@if(Session::has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{ Session::get('success') }}
		</div>
	@endif

	@if($errors->has())
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong>
			<ul>
				{{$errors->first('id', '<li>:message</li>')}}
				{{$errors->first('custname', '<li>:message</li>')}}
			</ul>
		</div>
	@endif

	

	{{Form::open('customers/'.$customer->id, 'PUT', array('class' => 'form-horizontal', 'id' => 'customerForm'))}}
		<div class='control-group'> 
			<label class='text'>
				<span class='ilabel'>Customer Id</span>				
				{{Form::text('id', $customer->id, array('class'=> 'span1', 'id' => 'id', 'disabled' => 'disabled'))}}	
			</label>
			<label class='text'>
				<span class='ilabel'>Nama Customer</span>
				{{Form::text('custname', $customer->custname, array('class'=> 'span4', 'id' =>'custname'))}}
			</label>
			<label class='text'>
				<span class='ilabel'>Nama Perusahaan</span>
				{{Form::text('company',$customer->company, array('class'=> 'span4', 'id' => 'company'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Alamat</span>
				{{Form::text('address',$customer->address, array('class'=> 'span4', 'id' => 'address'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Telp</span>
				{{Form::text('telp',$customer->telp, array('class'=> 'span4', 'id' => 'telp'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Email</span>
				{{Form::text('email',$customer->email, array('class'=> 'span4', 'id' => 'email'))}}
			</label>
			<br>
			<label class='text'>
				<span class='ilabel'>&nbsp;</span>
  				{{HTML::link(Request::referrer(), 'Back', array('class' => 'btn'))}}
  				{{HTML::link('#', 'Save', array('id' => 'saveButton', 'class' => 'btn btn-primary'))}}
			</label>
		</div>

	{{Form::close()}}

@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_customer");

		$('#saveButton').click(function(){ 
			if ($('#custname').val() === '') {
				alert('Silakan masukkan Nama Customer');
				return false;
			}

			if ($('#company').val() === '') {
				alert('Silakan masukkan Nama Perusahaan');
				return false;
			}

			if ($('#email').val() != '') {
				var sEmail = $('#email').val();
		        if (!validateEmail(sEmail)) {
		            alert('Alamat email tidak mengikuti format yang diharuskan');
		            return false;
		        }
		    }

			$('#id').removeAttr('disabled');
			$('form#customerForm').submit();
		});
    });

	function validateEmail(sEmail) {
	    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	    if (filter.test(sEmail)) {
	        return true;
	    }
	    else {
	        return false;
	    }
	}

</script>
@endsection	