@layout('master')

@section('container')
	{{Form::open('/customers', 'POST', array('class' => 'form-horizontal', 'id' => 'customerForm'))}}

		<div class='control-group'> 
			<label class='text'>
				<span class='ilabel'>Customer Id</span>
				{{Form::text('id',$lastId, array('class'=> 'span1', 'id' =>'id', 'disabled' => 'disabled'))}}	
			</label>
			<label class='text'>
				<span class='ilabel'>Nama Customer</span>
				{{Form::text('custname','', array('class'=> 'span1', 'id' => 'custname'))}}	
			</label>
			<label class='text'>
				<span class='ilabel'>Nama Perusahaan</span>
				{{Form::text('company','', array('class'=> 'span4', 'id' => 'company'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Alamat</span>
				{{Form::text('address','', array('class'=> 'span4', 'id' => 'address'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Telp.</span>
				{{Form::text('telp','', array('class'=> 'span4', 'id' => 'telp'))}}	
			</label>

			<label class='text'>
				<span class='ilabel'>Email</span>
				{{Form::text('email','', array('class' => 'span4', 'id' => 'email'))}}	
			</label>
			<br>
			<label class='text'>
				<span class='ilabel'>&nbsp;</span>
  				{{HTML::link_to_route('customers', 'Back', '', array('class' => 'btn'))}}
  				{{HTML::link('#', 'Reset', array('id' => 'resetButton', 'class' => 'btn btn-warning'))}}
  				&nbsp;
  				{{HTML::link('#', 'Save', array('id' => 'saveButton', 'class' => 'btn btn-primary'))}}
			</label>
		</div>

	{{Form::close()}}
	
@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_customer");

		$('#saveButton').click(function(){
			$('#id').removeAttr('disabled');

			if ($('#custname').val() === '') {
				alert('Silakan masukkan Nama Customer');
				return false;
			}

			if ($('#company').val() === '') {
				alert('Silakan masukkan Nama Perusahaan');
				return false;
			}

			if ($('#email').val() != '') {
				var sEmail = $('#email').val();
		        if (!validateEmail(sEmail)) {
		            alert('Alamat email tidak mengikuti format yang diharuskan');
		            return false;
		        }
		    }

			$('form#customerForm').submit();
			return false;
		});        

		$('#resetButton').click(function () {
			$("form#customerForm :input:not(:checkbox):not(:button):not([type=hidden]):not([name=id])").val('');
		});
	});

	function validateEmail(sEmail) {
	    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	    if (filter.test(sEmail)) {
	        return true;
	    }
	    else {
	        return false;
	    }
	}
</script>
@endsection