@layout('master')

@section('container')

	@if(Session::has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{ Session::get('success')}}
		</div>
	@endif

	@if(isset($success))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil!</strong>{{$success}}
		</div>
	@endif

  	{{ Form::open('search_customers', 'POST', array('id' => 'searchForm', 'class' => 'form-search pull-right'))}}
		<div class="input-append">
	  		<input type="text" name="searchText" id="searchText" class="input-medium search-query span3" , placeholder="Cari berdasarkan: Id/Cust.name/Company">
			<button type="submit" class="btn">Search</button>
		</div>
	{{ Form::close() }}

	<table class="table table-striped table-hover" id="customer_list">
		<thead>
		    <tr>
		      <th width="15%" class="num_title">Customer Id</th>
		      <th>Customer Name</th>
		      <th>Nama Perusahaan</th>
		      <th>Alamat</th>
		      <th width="20%">Telp</th>
		    </tr>
	    </thead>
	    <tbody>
	  	@foreach ($customers->results as $customer)
			<tr>
				<td class="num_id">
					{{$customer->id}}
				</td>
				<td>{{$customer->custname}}</td>
				<td>{{$customer->company}}</td>
				<td>{{$customer->address}}</td>
				<td>{{$customer->telp}}</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	{{$customers->links()}}
	{{HTML::link_to_route('new_customer', 'New Customer',null, array('class'=>'btn btn-primary'))}}
	<br>
	<br>

@endsection

@section('scripts')
	<script type="text/javascript">
	
	$(function() {
		setActiveMenu("menu_customer");
		var location =  "{{URL::to_route('customer')}}";

	    $("table#customer_list td").not(":first").click(function(){
	        document.location = location + "/" + 
	         $(this).closest("tr").find(".num_id").text();
	    });	

    });

</script>
@endsection

