<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Cost Printing Calculation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    {{HTML::script('js/jquery.js')}}
    {{Asset::container('bootstrapper')->styles()}}
    {{Asset::container('bootstrapper')->scripts()}}
    {{HTML::script('js/scrollTo.js')}}
    {{HTML::script('js/easel.js')}}
    {{HTML::script('js/autoNumeric.js')}}
    {{HTML::script('js/script.js')}}
    {{HTML::script('js/jquery.validate.js')}}
    <!-- Le styles -->
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
        background: #F5F6F6;
      }
    </style>
    {{HTML::style('css/style.css');}}

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
</head>
    <!-- Fav and touch icons -->
  <body data-feedly-processed="yes">
    <div id="wrap">

      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#">PRINT COST CALCULATION</a>
            <div class="nav-collapse collapse">
              <ul class="nav">
                <li class="active">{{HTML::link('/', 'Home')}}</li>
                <li id="menu_salesorder">{{HTML::link_to_route('salesorders', 'Sales Order')}}</li>
                <li class="dropdown" id='menu_print'>
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan / Cetak <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                  <li><a href="{{URL::to_route('salesorders')}}">Invoice</a></li>
                  <li><a href="{{URL::to_route('salesorders')}}">Surat Jalan</a></li>
                  <li><a href="{{URL::to_route('salesorders')}}">SPK</a></li>
                  </ul>
                </li>
                <li id="menu_customer">{{HTML::link_to_route('customers', 'Customer')}}</li>
                <li id="menu_user">{{HTML::link_to_route('users', 'User')}}</li>
              </ul>
              <div class="pull-right">
                <div class="span2 username">Hello, {{Auth::user()->name}}</div>
                <div class="span1">{{HTML::link('logout', 'Logout', array('class'=> 'btn btn-info'))}}</div> 
              </div>
            </div><!--/.nav-collapse -->
          </div>
        </div>
      </div>
      <div class="container">
        @yield('container')
      </div>
      @yield('scripts')
      <div id="push"></div>
  </div>

  <div id="footer">
    <div class="container">
      <p class="muted credit text-center">Courtesy of evLogia Solution @ 2013</p>
    </div>
  </div>

</body>
</html>