<?php

class User extends Eloquent 
{
    public static $timestamps = true;

    public static $rules = array(
        'name' => 'required|min:3',
        'password' => 'required|min:4|confirmed',
        'password_confirmation'=>'required|min:4',
        'email' => 'email');

    public function set_password($password)
	{
	    $this->set_attribute('password', Hash::make($password));
	}

    public static function validate($input) 
    {
    	$messages = array(
            'name_min' => 'Username harus lebih dari 3 karakter.',
            'name_required' => 'Username harus diisi.',
            'password_min' => 'Password harus lebih dari 3 karakter.',
            'password_required' => 'Password harus diisi.',
            'password_confirmation_min' => 'Confirm Password harus lebih dari 3 karakter.',
            'password_confirmation_reqiured' => 'Confirm Password harus diisi.', 
            'email_email' => 'Format email salah.',
        );

        return Validator::make($input, static::$rules, $messages);
    }

}