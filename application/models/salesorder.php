<?php

class Salesorder extends Eloquent 
{
    public static $timestamps = true;
    public static $rules = array(
        'id' => 'required|min:3');

    public static function validate($input) 
    {
    	$messages = array(
            'id_min' => 'Sales Order Id harus lebih dari 3 karakter.',
            'id_required' => 'Sales Order Id harus diisi.',
        );

        return Validator::make($input, static::$rules, $messages);
    }

    public function customer()
	{
		return $this->belongs_to('Customer');
	}

}