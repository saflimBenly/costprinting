<?php

class Customer extends Eloquent 
{
    public static $timestamps = true;
    public static $rules = array(
        'custname' => 'required|min:3',
        'company' => 'required|min:3',
        'email' => 'email');


    public static function validate($input) 
    {
    	$messages = array(
            'custname_min' => 'Customer Id harus lebih dari 3 karakter.',
            'custname_required' => 'Customer Id harus diisi.',
            'company_min' => 'Company Name harus lebih dari 3 karakter.',
            'company_required' => 'Company Name harus diisi.',
            'email_email' => 'Format email salah.',
        );

        return Validator::make($input, static::$rules, $messages);
    }

	public function salesorders()
	{
		return $this->has_many('Salesorder');
	}

	public static function getCustomerForSelect()
	{
        $customers = static::all();
        $arrCust = array();
        foreach ($customers as $customer) {
            $arrCust[] = '"'.$customer->id.'-'.$customer->company.'"';
        }
        return '['. implode(",", $arrCust) .']';
        // return '["Alabama","Alaska","Arizona","Arkansas"]';

	}
}