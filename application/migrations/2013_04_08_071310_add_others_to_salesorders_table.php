<?php

class Add_Others_To_Salesorders_Table {    

	public function up()
    {
		Schema::table('salesorders', function($table) {
			$table->string('other');
			$table->integer('total_other');
	});

    }    

	public function down()
    {
		Schema::table('salesorders', function($table) {
			$table->drop_column(array('other', 'total_other'));
	});

    }

}