<?php

class Delete_Finishingcost_From_Salesorders_Table {    

	public function up()
    {
		Schema::table('salesorders', function($table) {
			$table->drop_column('finishingcost');
	});

    }    

	public function down()
    {
		Schema::table('salesorders', function($table) {
			$table->integer('finishingcost');
	});

    }

}