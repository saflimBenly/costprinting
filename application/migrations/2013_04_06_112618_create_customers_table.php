<?php

class Create_Customers_Table {    

	public function up()
    {
		Schema::create('customers', function($table) {
			$table->increments('id');
			$table->string('custname');
			$table->string('company');
			$table->string('address');
			$table->string('telp');
			$table->string('email');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('customers');

    }

}