<?php

class Create_Users_Table {    

	public function up()
    {
		Schema::create('users', function($table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->string('email');
			$table->string('position')->nullable();
			$table->string('gender');
			$table->string('password');
			$table->text('bio')->nullable();
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('users');

    }

}