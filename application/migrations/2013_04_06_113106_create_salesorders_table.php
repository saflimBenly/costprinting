<?php

class Create_Salesorders_Table {    

	public function up()
    {
		Schema::create('salesorders', function($table) {
			$table->increments('id');
			$table->integer('customer_id');
			$table->string('printname');
			$table->integer('qty');
			$table->string('finishing');
			$table->integer('finishingcost');
			$table->integer('total');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('salesorders');

    }

}