<?php

class Add_Fields_To_Salesorders_Table {    

	public function up()
    {
		Schema::table('salesorders', function($table) {
			$table->string('picturedimension');
			$table->string('plat');
			$table->integer('total_finishing');
			$table->integer('total_pisau');
			$table->integer('total_pon');
			$table->integer('total_potong');
			$table->integer('total_bor');
			$table->integer('total_mataayam');
			$table->integer('total_tali');
			$table->integer('total_klise');
	});

    }    

	public function down()
    {
		Schema::table('salesorders', function($table) {
			$table->drop_column(array('picturedimension', 'plat', 'total_pisau', 'total_pon', 'total_potong', 'total_bor', 'total_mataayam', 'total_tali', 'total_klise', 'total_finishing'));
	});

    }

}