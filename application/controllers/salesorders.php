<?php

class Salesorders_Controller extends Base_Controller {    

	public function action_index()
    {
        Session::flush();
        Log::write('info', '>>> the search text: ' . '%'. Input::get('searchText', '') .'%');

        $query = DB::table('salesorders')
                        ->select(array('salesorders.id AS salesorders_id', 'customers.custname', 'salesorders.printname', 'salesorders.qty' , 'salesorders.total'))
                        ->where('salesorders.id', 'like', '%'. Input::get('searchText', '') .'%')
                        ->or_where('salesorders.printname', 'like', '%'. Input::get('searchText', '') .'%');

        // Adds conditional join and where
        $query->join('customers', 'customers.id', '=', 'salesorders.customer_id')
            ->or_where('customers.company', 'like',  '%'. Input::get('searchText', '') .'%');

        // Adds another clause
        $query->order_by('salesorders_id','asc');

        // Fecthes results
        $salesorders = $query->paginate(5);

        // OLD method
        // $salesorders = Salesorder::with(
        //     array('customer' => 
        //         function($query){
        //             $query->where('company', 'like', '%'. Input::get('searchText', '') .'%');
        //             }
        //         ))
        // ->order_by('id','asc')
        // ->where('id', 'like', '%'. Input::get('searchText', '') .'%')
        // ->or_where('printname', 'like', '%'. Input::get('searchText', '') .'%')
        // ->paginate(5);

        // dd($salesorders);
        return View::make('salesorder.index')->with('salesorders', $salesorders);
    }    

	public function action_show($id)
    {
        return View::make('salesorder.show')->with('salesorder', Salesorder::find($id));
    }    

	public function action_new()
    {

        $lastSalesOrder = Salesorder::order_by('id', 'desc')->first();
        $getCustomerForSelect = Customer::getCustomerForSelect();
        return View::make('salesorder.new')->with('getCustomerForSelect', $getCustomerForSelect)->with('lastId', $lastSalesOrder->id + 1);
    } 

	public function action_edit($id)
    {
        $salesorder = new Salesorder;
        if (Session::has('validate') && Session::get('validate') == 'error') {
            Log::write('info', '>>> Error:' . Input::old('total_finishing'));
            Session::put('validate', '');
            $salesorder->fill(array(
                'id' => Input::old('id'),
                'printname' => Input::old('printname'),
                'qty' => Input::old('qty'),
                'finishing' => Input::old('finishing'),
                'total' => Input::old('total'),
                'picturedimension' => Input::old('picturedimension'),
                'plat' => Input::old('plat'),
                'total_finishing' => Input::old('total_finishing'),
                'total_pisau' => Input::old('total_pisau'),
                'total_pon' => Input::old('total_pon'),
                'total_potong' => Input::old('total_potong'),
                'total_bor' => Input::old('total_bor'),
                'total_mataayam' => Input::old('total_mataayam'),
                'total_tali' => Input::old('total_tali'),
                'total_klise' => Input::old('total_klise'),
                'other' => Input::old('other'),
                'total_other' => Input::old('total_other'),
                'total' => Input::old('total'),
                )); 
            $salesorder->customer = Customer::find(1);

        Log::write('info', '>>> Result:' . $salesorder->id . ';' . $salesorder->total_finishing);
        } else {
            Log::write('info', '>>> No Error');
            $salesorder = Salesorder::find($id); 
        }
        
        return View::make('salesorder.edit')->with('salesorder', $salesorder);
    }

    public function action_create()
    {
        $validation = Salesorder::validate(Input::all());
        $id = Input::get('id'); 

        Log::write('info', 'Create SO!!');

        if($validation->fails()){
            Session::put('validate', 'error');
            return Redirect::to('salesorders/'. $id .'/edit')
                ->with_errors($validation)
                ->with_input();
        }

        Customer::find(Input::get('custId'))
                ->salesorders()
                ->insert(array(
                    'id' => $id,
                    'printname' => Input::get('printname'),
                    'qty' => Input::get('qty'),
                    'finishing' => Input::get('finishing'),
                    'total' => Input::get('total'),
                    'picturedimension' => Input::get('picturedimension'),
                    'plat' => Input::get('plat'),
                    'total_finishing' => Input::get('total_finishing'),
                    'total_pisau' => Input::get('total_pisau'),
                    'total_pon' => Input::get('total_pon'),
                    'total_potong' => Input::get('total_potong'),
                    'total_bor' => Input::get('total_bor'),
                    'total_mataayam' => Input::get('total_mataayam'),
                    'total_tali' => Input::get('total_tali'),
                    'total_klise' => Input::get('total_klise'),
                    'other' => Input::get('other'),
                    'total_other' => Input::get('total_other'),
                    'total' => Input::get('total'),
                ));
        Log::write('info', 'Create success!');

        return Redirect::to(URL::to_route('salesorder', $id))->with('success', ' Sales order dengan no.' . $id .' berhasil disimpan.');
    } 

    public function action_update($id)
    {
        $validation = Salesorder::validate(Input::all());
        // dd($validation);
        Log::write('info', '>>> total_finishing:' . Input::get('total_finishing'));

        if($validation->fails()){
            Log::write('info', '>>> Fails!!');
            Session::put('validate', 'error');
            return Redirect::to('salesorders/'.$id.'/edit')
                ->with_errors($validation)
                ->with_input();
        } else {
            Log::write('info', '>>> prepare save!');

            $salesorder = Salesorder::find(Input::get('id'));
            $salesorder->printname = Input::get('printname');
            $salesorder->qty = Input::get('qty');
            $salesorder->finishing = Input::get('finishing');
            $salesorder->total = Input::get('total');
            $salesorder->picturedimension = Input::get('picturedimension');
            $salesorder->plat = Input::get('plat');
            $salesorder->total_finishing = Input::get('total_finishing');
            $salesorder->total_pisau = Input::get('total_pisau');
            $salesorder->total_pon = Input::get('total_pon');
            $salesorder->total_potong = Input::get('total_potong');
            $salesorder->total_bor = Input::get('total_bor');
            $salesorder->total_mataayam = Input::get('total_mataayam');
            $salesorder->total_tali = Input::get('total_tali');
            $salesorder->total_klise = Input::get('total_klise');
            $salesorder->other = Input::get('other');
            $salesorder->total_other = Input::get('total_other');
            $salesorder->total = Input::get('total');
            $salesorder->save();

            Log::write('info', '>>> End!!');
            return Redirect::to(URL::to_route('salesorder', $id))->with('success', ' Sales order dengan no.' . $id .' berhasil diupdate.');
        }
    } 

    public function action_destroy($id)
    {

        if (!($salesorder = Salesorder::find($id)))
            return Response::error('404');

        $salesorder->delete();

        return Redirect::to('salesorders/')->with('success', ' Sales order dengan no.' . $id .' berhasil dihapus.');
    }
}