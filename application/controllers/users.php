<?php

class Users_Controller extends Base_Controller {    

    public function action_index()
    {
        Session::flush();

        $users = User::order_by('id','asc')
        ->where('id', 'like', '%'. Input::get('searchText', '') .'%')
        ->or_where('name', 'like', '%'. Input::get('searchText', '') .'%')
        ->paginate(5);

        return View::make('user.index')->with('users', $users);
    }    

    public function action_show($id)
    {
        return View::make('user.show')->with('user', User::find($id));
    }    

    public function action_new()
    {
        $lastuser = User::order_by('id', 'desc')->first();
        return View::make('user.new')->with('lastId', $lastuser->id + 1);
    }    

	public function action_edit()
    {

    }

}