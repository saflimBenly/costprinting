<?php

class Customers_Controller extends Base_Controller {    

    public function action_index()
    {
        Session::flush();

        $customers = Customer::order_by('id','asc')
        ->where('id', 'like', '%'. Input::get('searchText', '') .'%')
        ->or_where('custname', 'like', '%'. Input::get('searchText', '') .'%')
        ->or_where('company', 'like', '%'. Input::get('searchText', '') .'%')
        ->paginate(5);

        return View::make('customer.index')->with('customers', $customers);
    }    

    public function action_show($id)
    {
        return View::make('customer.show')->with('customer', Customer::find($id));
    }    

	public function action_new()
    {
        $lastCustomer = Customer::order_by('id', 'desc')->first();
        return View::make('customer.new')->with('lastId', $lastCustomer->id + 1);
    }    

    public function action_edit($id)
    {
        $customer = new Customer;
        if (Session::has('validate') && Session::get('validate') == 'error') {
            Log::write('info', '>>> Error validate.');
            Session::put('validate', '');
            $customer->fill(array(
                'id' => Input::old('id'),
                'custname' => Input::old('custname'),
                'company' => Input::old('company'),
                'address' => Input::old('address'),
                'telp' => Input::old('telp'),
                'email' => Input::old('email'),
                )); 
            $customer->customer = Customer::find(1);

        Log::write('info', '>>> Result:' . $customer->id);
        } else {
            Log::write('info', '>>> No Error');
            $customer = Customer::find($id); 
        }
        
        return View::make('customer.edit')->with('customer', $customer);
    }


    public function action_create()
    {
        $validation = Customer::validate(Input::all());
        $id = Input::get('id'); 

        Log::write('info', 'Create Customer!!');

        if($validation->fails()){
            Log::write('info', 'Customer error!');
            Session::put('validate', 'error');
            return Redirect::to('customers/'. $id .'/edit')
                ->with_errors($validation)
                ->with_input();
        }

        $user = Customer::create(array(
                    'id' => $id,
                    'custname' => Input::get('custname'),
                    'company' => Input::get('company'),
                    'address' => Input::get('address'),
                    'telp' => Input::get('telp'),
                    'email' => Input::get('email'),
                ));
        Log::write('info', 'Create success!');

        return Redirect::to(URL::to_route('customer', $id))->with('success', ' Customer dengan no.' . $id .' berhasil disimpan.');
    } 

    public function action_update($id)
    {
        $validation = Customer::validate(Input::all());
        // dd($validation);
        Log::write('info', '>>> To Validate');

        if($validation->fails()){
            Log::write('info', '>>> Fails!!');
            Session::put('validate', 'error');
            return Redirect::to('customers/'.$id.'/edit')
                ->with_errors($validation)
                ->with_input();
        } else {
            Log::write('info', '>>> prepare save!');

            $customer = Customer::find(Input::get('id'));
            $customer->custname = Input::get('custname');
            $customer->company = Input::get('company');
            $customer->address = Input::get('address');
            $customer->telp = Input::get('telp');
            $customer->email = Input::get('email');
            $customer->save();

            Log::write('info', '>>> End!!');
            return Redirect::to(URL::to_route('customer', $id))->with('success', ' Customer dengan no.' . $id .' berhasil diupdate.');
        }
    } 

    public function action_destroy($id)
    {

        if(Salesorder::where('customer_id', '=', $id)->first()) {
            Log::write('info', 'Can\'t delete that!');
            $parent = 'Customer masih terkait dengan table lain (Sales Order).';
            return View::make('customer.show')->with('customer', Customer::find($id))->with('parent' , $parent);
        }

        if (!($customer = Customer::find($id)))
            return Response::error('404');

        $customer->delete();

        $success = 'Customer dengan no.' . $id .' berhasil dihapus.';
        return Redirect::to('customers/')->with('success', $success);
    }
}